<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->id();
            $table->string('source')->nullable()->index();
            $table->string('courseid')->nullable()->index();
            $table->string('component')->nullable();
            $table->string('activityid')->nullable()->index();
            $table->integer('grade')->nullable();
            $table->string('student_ids')->nullable()->index();
            $table->string('directory')->nullable();
            $table->string('working_directory')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
