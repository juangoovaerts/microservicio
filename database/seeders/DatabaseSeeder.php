<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'name' => 'imaster',
            'email' => 'imaster@imaster.com',
            'password' => bcrypt('iMaster2020')
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Juan Goovaerts',
            'email' => 'juangoovaerts@gmail.com',
            'password' => bcrypt('password')
        ]);
    }
}
