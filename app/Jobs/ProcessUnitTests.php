<?php

namespace App\Jobs;

use App\Models\Grade;
use App\Models\GradeIntent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;

class ProcessUnitTests implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $workingDirectory;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 1700;
    /**
     * @var array
     */
    public $params;
    public $directory;
    public $grade_intent;

    /**
     * Create a new job instance.
     *
     * @param $workingDirectory
     * @param $directory
     * @param array $params
     */
    public function __construct($workingDirectory, $directory,$params = [])
    {
        //
        $this->workingDirectory = $workingDirectory;
        $this->params = $params;
        $this->directory = $directory;
        $this->grade_intent = GradeIntent::create([]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->setGradeIntent();
        $this->installDependencies();
        $this->ensureTestScriptForReact();
        $this->runTests();
    }

    public function ensureTestScriptForReact() {
        $packages = json_decode(file_get_contents(
            $this->workingDirectory . '/package.json'
          ), true);
            if(array_key_exists('react', $packages['dependencies'])) {
                $packages['scripts']['test'] = 'react-scripts test --json --watchAll=false';

                file_put_contents(
                $this->workingDirectory . '/package.json',
                json_encode($packages, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT).PHP_EOL
            );
        }
    }

    public function setGradeIntent()
    {
        $source = $this->params['source'];
        $courseid = $this->params['courseid'];
        $component = $this->params['component'];
        $activityid = $this->params['activityid'];
        $this->grade_intent->update([
            'source' => $source,
            'courseid' => $courseid,
            'component' => $component,
            'activityid' => $activityid,
            'directory' => $this->directory,
            'working_directory' => $this->workingDirectory,
            'student_ids' => json_encode($this->params['student_ids'])
        ]);
    }

    public function removeNodeModulesIfAny()
    {
        //REMOVE NODE_MODULES IF ANY IN ROOT OF EVALUATION
        try {
            $root_path = storage_path('app/evaluaciones');
            $process = new Process(['rm', '-r', 'node_modules']);
            $process->setWorkingDirectory(
                $root_path
            );
            $process->run();
            while ($process->isRunning()) {
            }
            $process = new Process(['rm', 'package-lock.json']);
            $process->setWorkingDirectory(
                $root_path
            );
            $process->run();
            while ($process->isRunning()) {
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }


        $process = new Process(['rm', '-r', 'node_modules']);
        $process->setWorkingDirectory(
            $this->workingDirectory
        );
        $process->run();
        while ($process->isRunning()) {
        }

        $process = new Process(['rm', 'package-lock.json']);
        $process->setWorkingDirectory(
            $this->workingDirectory
        );
        $process->run();
        while ($process->isRunning()) {
        }
    }

    public function installDependencies()
    {
        Log::info('------------------------------------');
        Log::info($this->directory);
        Log::info(json_encode($this->params['student_ids']));
        Log::info('Removing cache');
        $this->removeNodeModulesIfAny();
        Log::info('Starting npm install');
        $process = new Process(['npm', 'install']);
        $process->setTimeout(1000);
        $process->setWorkingDirectory(
            $this->workingDirectory
        );
        $process->run();
        while ($process->isRunning()) {
        }
        Log::info('Installed');
    }

    public function runTests()
    {
        Log::info('Starting npm run test');
        $process = new Process(['npm', 'run', 'test']);
        $process->setTimeout(500);
        $process->setWorkingDirectory(
            $this->workingDirectory
        );
        $output = '';
        $process->run();
        while ($process->isRunning()) {
        }
        Log::info('Finished running tests');
        $output = $process->getOutput();
        Log::info($output . ' <---');
        $number = strpos($output, '{');
        $json = substr($output, $number);
        $fin = strripos($json,"}");
        $jsonFinal =  substr($json, 0, $fin + 1);
        $score = json_decode($jsonFinal);
        $total = $score->numTotalTests;
        $passed = $score->numPassedTests;
        $grade = round(($passed / $total) * 10 / 2, 2);
        $source = $this->params['source'];
        $courseid = $this->params['courseid'];
        $component = $this->params['component'];
        $activityid = $this->params['activityid'];
        Grade::create([
            'source' => $source,
            'courseid' => $courseid,
            'component' => $component,
            'activityid' => $activityid,
            'grade' => $grade,
            'directory' => $this->directory,
            'working_directory' => $this->workingDirectory,
            'student_ids' => json_encode($this->params['student_ids'])
        ]);

        Log::info('Guardado en base de datos');
        if(env('SEND_GRADES')) {
            $this->sendGrades($grade);
        }
    }

    public function sendGrades($score)
    {
        Log::info('Starting generating url');
        //CONSTANTES
        $url = "https://imaster.academy/webservice/rest/server.php";
        $token = "7bea57cca1cc2b80c48d1997ee7f6668";
        $function = "core_grades_update_grades";
        // VARIABLES
        $source = $this->params['source'];
        $courseid = $this->params['courseid'];
        $component = $this->params['component'];
        $activityid = $this->params['activityid'];
        // TODO VER COMO MANDAR TODO DE UNA
        $grade = $score;
        $urlPost =
            "{$url}?wstoken={$token}&wsfunction={$function}&moodlewsrestformat=json&source={$source}&courseid={$courseid}&component={$component}&activityid={$activityid}&itemnumber=0";
        foreach($this->params['student_ids'] as $key => $student_id) {
            $urlPost = $urlPost . "&grades[{$key}][studentid]={$student_id}&grades[{$key}][grade]={$grade}";
        }
        Log::info('url:  ' . $urlPost);
        //dump($urlPost);
        $response = Http::get($urlPost);
        Log::info('Finnished ');
        Log::info(json_encode($response->body()));
        Log::info($response->successful() ? 'Success' : 'Failed');
        $reponseCode = json_encode($response->body());
        Log::info('responseCode: ' . $reponseCode);
        if(strpos($reponseCode, "0")){
            Log::info('Starting removing node_modules');
            $process = new Process(['rm', '-r', 'node_modules']);
            $process->setWorkingDirectory(
                $this->workingDirectory
            );
            $output = '';
            $process->run();
            while ($process->isRunning()) {
            }
            Log::info('Finished  removing node_modules');
            Log::info('Starting  removing directory');
            Storage::deleteDirectory('evaluaciones/' . $this->directory);
            Log::info('Finished  removing directory');
        } else {
            throw new \Exception('Not successfull');
            Log::error('No funcionó el web service');
        }
        Log::info('------------------------------------');
    }


}
