<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeApiToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a api token to access micro service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::where('name', 'imaster')->first();
        if(!$user) return 1;
        $token = $user->createToken('test:process');
        $this->line('Aquí esta tu access token');
        $this->info($token->plainTextToken);
        return 0;
    }
}
