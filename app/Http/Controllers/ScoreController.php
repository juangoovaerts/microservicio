<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessUnitTests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use VIPSoft\Unzip\Unzip;
use ZanySoft\Zip\Zip;
use ZipArchive;

class ScoreController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'source' => ['required'],
            'courseid' =>  ['required'],
            'component' =>  ['required'],
            'activityid' =>  ['required'],
            'student_ids' => ['array', 'required', 'min:1']
        ]);
        if(isset($request->zipfile)){
            $unzipper  = new Unzip();
            $file = $request->zipfile->store('zips'); //store file in storage/app/zip
            $zip = Zip::open(storage_path('app/'.$file));
            $zip->extract(storage_path('app/evaluaciones'));
            $files = $zip->listFiles();
            $params = [
                'source' => $request->source,
                'courseid' => $request->courseid,
                'component' => $request->component,
                'activityid' => $request->activityid,
                'student_ids' => $request->student_ids
            ];
            $pos = 0;
            foreach ($files as $key => $file) {
                if(strpos($file, 'package.json')) {
                    if(!strpos($file, 'MACOSX')) {
                        $pos = $key;
                    }
                };
            }
            $directory = str_replace('/package.json', '', $files[$pos]);
            $workingDirectory = storage_path('app/evaluaciones/' . $directory);
            ProcessUnitTests::dispatch($workingDirectory, $directory, $params);
        }
        return 'success';
    }
}
