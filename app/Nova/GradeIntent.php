<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;


class GradeIntent extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var  string
     */
    public static $model = \App\Models\GradeIntent::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var  string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var  array
     */
    public static $search = [
        'id', 'source', 'courseid', 'activityid', 'component', 'student_ids', 'failed'
    ];

    /**
     * Get the displayable label of the resource.
     *
     * @return  string
     */
    public static function label()
    {
        return __('Grade Intents');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return  string
     */
    public static function singularLabel()
    {
        return __('Grade Intent');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return  array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('Id'), 'id')
                ->rules('required')
                ->sortable()
            ,
            Text::make(__('Source'), 'source')
                ->sortable()
            ,
            Text::make(__('Courseid'), 'courseid')
                ->sortable()
            ,
            Text::make(__('Component'), 'component')
                ->sortable()
            ,
            Text::make(__('Activityid'), 'activityid')
                ->sortable()
            ,
            Code::make(__('Student Ids'), 'student_ids')
                ->sortable()
                ->json()
            ,
            Text::make(__('Directory'), 'directory')
                ->sortable()
            ,
            Text::make(__('Working Directory'), 'working_directory')
                ->sortable()
            ,
            Text::make(__('Exception'), 'exception')
                ->sortable()
            ,
            Boolean::make(__('Failed'), 'failed')
                ->sortable()
            ,
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return  array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return  array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\IsFailed()
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return  array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return  array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
